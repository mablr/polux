# Polux : système de poulailler autonome

Polux est un projet permettant la garde autonome et supervisée d'un poulallier.Il permet d'ouvrir le poulailler le matin afin que les poules puissent gambader en plein air, et de le fermer le soir pour mettre à l'abri les gallinacés des prédateurs nocturnes.

## Matériel
(Prix indicatifs)
- Raspberry pi 3 A+ : 21.63€
- Pi cam : 23.99€
- Carte SD : 9.59€
- Boîtier dérivation : 5.37€
- Rallonge : 12.17€
- Fiche électrique : 11.28€
- Moteur pas à pas Nema17 : 16.99€
- Arduino pro micro : 6.66€
- Driver A4988 : 1.99€
- Roulements à billes 688zz : 6.19€
- Fils de câblage : 13.90€
- Pâte à souder : 7.27€
- Tresse à dessouder : 2.77€
- Gaines thermo : 10.99 €
- Étain : 10.99€

## Mise en place
### Schéma électronique
*Bientôt disponible*

### Arduino
Récupérer la bibliotèque DHT.h de Adafruit, pour le capteur de température.
Compiler le code et téléverser le code du dossier `arduino` sur votre carte. 

### Raspberry Pi
On considère que l'on débute avec une installation propre de Raspbian Lite.
#### Création d'un utilisateur dédié
```bash
adduser polux
adduser polux dialout # Pour avoir accès à l'arduino
```

#### Installation des paquets
```bash
apt update; apt upgrade -y; apt install -y nginx python3-venv
```

#### Environnement Python
```bash
su - polux
git clone ce_depot
cd polux/rpi
python3 -m virtualenv -p python3 venv3
. ./venv3/bin/activate
pip install -r requirements.txt
```

#### Déploiement de l'application
Copier cette configuration (en adaptant si nécessaire) dans le fichier `/etc/systemd/system/polux.service`
```
[Unit]
Description=Polux WebApp
After=network.target

[Service]
User=polux
Group=polux
WorkingDirectory=/home/polux/polux/rpi/webapp
Environment="PATH=/home/polux/polux/rpi/venv3/bin"
ExecStart=/home/polux/polux/rpi/venv3/bin/uwsgi -i polux.ini
Restart=on-failure
RuntimeMaxSec=600

[Install]
WantedBy=multi-user.target
```

Puis activer et lancer le nouveau service.
```bash
systemctl daemon-reload
systemctl start polux
systemctl enable polux
```

#### Configuration Nginx
Créer le fichier `/etc/nginx/sites-available/polux` (à adapter selon les besoins) :
```
server {
    listen 80;
    server_name 0.0.0.0;

    client_max_body_size 200M;
    location / {
        auth_basic "Panneau de controle Polux : acces restreint";
        auth_basic_user_file /etc/nginx/.htpasswd;
        include uwsgi_params;
        uwsgi_pass unix:/home/polux/polux/rpi/webapp/polux.sock;
    }
}
```

```bash
cd /etc/nginx/sites-enabled/
ln -s ../sites-available/polux
nginx -t
systemctl reload nginx
```

#### Crontab
Pour avoir une fermeture et ouverture automatique du poulailler, il faut ajouter 2 lignes à la table cron de l'utilisateur polux faisant appel à l'utilitaire en ligne de commande :
```bash
crontab -e
```
Ajouts:
```
# Ouverture de la porte à 7h tous les jours
0 7 * * * /home/polux/polux/rpi/cli/polux-cli.py open > /dev/null 2>&1
# Fermeture de la porte à 22h30 tous les jours
30 22 * * * /home/polux/polux/rpi/cli/polux-cli.py close > /dev/null 2>&1
```
