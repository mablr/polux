#!/usr/bin/env python3
import serial
import time
import sys
availableCmd = ['state','close','open','temp']

if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
    ser.flush()
    cmd = str(sys.argv[1])
    if cmd in availableCmd:
        ser.write((cmd+"\n").encode('utf-8'))
        while True: # Emulating DoWhile
            rep = ser.readline().decode('utf-8').rstrip()
            if rep == "#":
                break
            print(rep)
    else:
        print(cmd + " is not an available command")
        print("Please choose in this list: " + " ".join(availableCmd))
