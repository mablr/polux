#include "DHT.h"
#define DHTPIN 15
#define DHTTYPE DHT11
#define DirPin 2
#define StepPin 3
#define SPR 200
#define disableStep 9
#define rotCount 11
// Inspired by examples found just here : https://lastminuteengineers.com/a4988-stepper-motor-driver-arduino-tutorial/ and adafruit documentation for DHT library.

DHT dht(DHTPIN, DHTTYPE);
bool DoorOpened = false; 

void setup()
{
  Serial.begin(9600);
  // Make pins as Outputs
  pinMode(StepPin, OUTPUT);
  pinMode(DirPin, OUTPUT);
  pinMode(disableStep, OUTPUT);
  digitalWrite(disableStep, HIGH); //disable stepper
  dht.begin();
}
void loop()
{
  if (Serial.available() > 0) {
    String cmd = Serial.readStringUntil('\n');
    if (cmd == "close") {
      if (DoorOpened) {
        digitalWrite(disableStep, LOW); //enable stepper
        digitalWrite(DirPin, HIGH);   // defines the direction to clockwise
        // Pulse the step pin
        for(int x = 0; x < rotCount*SPR; x++) {
          digitalWrite(StepPin, HIGH);
          delayMicroseconds(1000);
          digitalWrite(StepPin, LOW);
          delayMicroseconds(1000);
        }
        digitalWrite(disableStep, HIGH); //disable stepper
        DoorOpened = false;
        Serial.println("Porte Fermée");
        Serial.println("#");
      }else{
        Serial.println("Déjà fermé");
        Serial.println("#");

      }
    }
    if (cmd == "open") {
      if (!DoorOpened) {
        digitalWrite(disableStep, LOW); //enable stepper
        digitalWrite(DirPin, LOW);   // defines the direction to counterclockwise
        // Pulse the step pin
        for(int x = 0; x < rotCount*SPR; x++) {
          digitalWrite(StepPin, HIGH);
          delayMicroseconds(1000);
          digitalWrite(StepPin, LOW);
          delayMicroseconds(1000);
        }
        digitalWrite(disableStep, HIGH); //disable stepper
        DoorOpened = true;
        Serial.println("Porte Ouverte");
        Serial.println("#");
      }else{
        Serial.println("Déjà ouvert");
        Serial.println("#");
      }
    }
    if (cmd == "state") {
      if (DoorOpened) {
        Serial.println("Porte Ouverte");
      }else{
        Serial.println("Porte Fermée");
      }
      Serial.println("#");
    }
    if (cmd == "temp") {
      float h = dht.readHumidity();
      float t = dht.readTemperature();
      float hic = dht.computeHeatIndex(t, h, false);
      Serial.print("Humidity: ");
      Serial.print(h);
      Serial.print("%  Temperature: ");
      Serial.print(hic);
      Serial.println(F("°C "));
      Serial.println("#");
    }
  }
}
