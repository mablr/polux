#!/usr/bin/env python3
""" Pi camera part was written by Miguel Grinberg, https://github.com/miguelgrinberg/flask-video-streaming, MIT Licence """
from flask import Flask, render_template, redirect, request, Response
import serial
import os, fnmatch
import io
import time
from picamera import PiCamera
import threading
from crontab import CronTab

availableCmd = ['state','close','open','temp']
openCmd = os.getcwd()+'/../cli/polux-cli.py open > /dev/null 2>&1'
closeCmd = os.getcwd()+'/../cli/polux-cli.py close > /dev/null 2>&1'
cron = CronTab(user=True)
app = Flask(__name__)

try:
    from greenlet import getcurrent as get_ident
except ImportError:
    try:
        from thread import get_ident
    except ImportError:
        from _thread import get_ident


class CameraEvent(object):
    """An Event-like class that signals all active clients when a new frame is
    available.
    """
    def __init__(self):
        self.events = {}

    def wait(self):
        """Invoked from each client's thread to wait for the next frame."""
        ident = get_ident()
        if ident not in self.events:
            # this is a new client
            # add an entry for it in the self.events dict
            # each entry has two elements, a threading.Event() and a timestamp
            self.events[ident] = [threading.Event(), time.time()]
        return self.events[ident][0].wait()

    def set(self):
        """Invoked by the camera thread when a new frame is available."""
        now = time.time()
        remove = None
        for ident, event in self.events.items():
            if not event[0].isSet():
                # if this client's event is not set, then set it
                # also update the last set timestamp to now
                event[0].set()
                event[1] = now
            else:
                # if the client's event is already set, it means the client
                # did not process a previous frame
                # if the event stays set for more than 5 seconds, then assume
                # the client is gone and remove it
                if now - event[1] > 5:
                    remove = ident
        if remove:
            del self.events[remove]

    def clear(self):
        """Invoked from each client's thread after a frame was processed."""
        self.events[get_ident()][0].clear()


class BaseCamera(object):
    thread = None  # background thread that reads frames from camera
    frame = None  # current frame is stored here by background thread
    last_access = 0  # time of last client access to the camera
    event = CameraEvent()

    def __init__(self):
        """Start the background camera thread if it isn't running yet."""
        if BaseCamera.thread is None:
            BaseCamera.last_access = time.time()

            # start background frame thread
            BaseCamera.thread = threading.Thread(target=self._thread)
            BaseCamera.thread.start()

            # wait until frames are available
            while self.get_frame() is None:
                time.sleep(0)

    def get_frame(self):
        """Return the current camera frame."""
        BaseCamera.last_access = time.time()

        # wait for a signal from the camera thread
        BaseCamera.event.wait()
        BaseCamera.event.clear()

        return BaseCamera.frame

    @staticmethod
    def frames():
        """"Generator that returns frames from the camera."""
        raise RuntimeError('Must be implemented by subclasses.')

    @classmethod
    def _thread(cls):
        """Camera background thread."""
        print('Starting camera thread.')
        frames_iterator = cls.frames()
        for frame in frames_iterator:
            BaseCamera.frame = frame
            BaseCamera.event.set()  # send signal to clients
            time.sleep(0)

            # if there hasn't been any clients asking for frames in
            # the last 10 seconds then stop the thread
            if time.time() - BaseCamera.last_access > 10:
                frames_iterator.close()
                print('Stopping camera thread due to inactivity.')
                break
        BaseCamera.thread = None

class Camera(BaseCamera):
    @staticmethod
    def frames():
        with PiCamera() as camera:
            # let camera warm up
            time.sleep(2)

            stream = io.BytesIO()
            camera.resolution = (320, 240)
            camera.vflip = True
            for _ in camera.capture_continuous(stream, 'jpeg',
                                                 use_video_port=True):
                # return current frame
                stream.seek(0)
                yield stream.read()

                # reset stream for next frame
                stream.seek(0)
                stream.truncate()

def findSerialDev(pattern):
    for root, dirs, files in os.walk('/dev'):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                return os.path.join(root, name)

def serialWrite(cmd):
    rep = ""
    ser.write((cmd+"\n").encode('utf-8'))
    while True: # Emulating DoWhile
        buf = ser.readline().decode('utf-8').rstrip()
        if buf == "#":
            break
        rep += buf
    return rep

@app.route('/')
def index():
    return redirect("/polux", code=302)

@app.route('/polux', methods=['GET','POST'])
def polux():
    noCmd = None
    noCron = None
    error = None
    feedback = None
    openJob = None
    closeJob = None
    openJobs = cron.find_command(openCmd)
    closeJobs = cron.find_command(closeCmd)
    if openJobs:
        openJob = next(openJobs, None)
    if closeJobs:
        closeJob = next(closeJobs, None)
    if request.method == 'POST':
        try:
            if request.form['cmd'] in availableCmd:
                feedback = serialWrite(request.form['cmd'])
        except:
           noCmd = True
        try:
            request.form['hopen']
            request.form['mopen']
            request.form['hclose']
            request.form['mclose']
        except:
            noCron = True
        else:
            if int(request.form['hopen']) >= 0 and int(request.form['hopen']) < 24 and int(request.form['mopen']) >= 0 and int(request.form['mopen']) < 60 and int(request.form['hclose']) >= 0 and int(request.form['hclose']) < 24 and int(request.form['mclose']) >= 0 and int(request.form['mclose']) < 60:
                cron.remove_all(command=openCmd)
                cron.remove_all(command=closeCmd)
                openJob = cron.new(command=openCmd)
                openJob.hour.on(request.form['hopen'])
                openJob.minute.on(request.form['mopen'])
                openJob.enable()
                closeJob = cron.new(command=closeCmd)
                closeJob.hour.on(request.form['hclose'])
                closeJob.minute.on(request.form['mclose'])
                closeJob.enable()
                cron.write()
                feedback="Heure d'ouverture/fermeture modifiée"
        if noCron and noCmd:
            error = "Empty POST request!"
    return render_template('index.html', feedback=feedback, error=error, openJob=str(openJob), closeJob=str(closeJob))


def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(gen(Camera()), mimetype='multipart/x-mixed-replace; boundary=frame')

ser = serial.Serial(findSerialDev('ttyACM*'), 9600, timeout=1)

if __name__ == '__main__':
    app.run()

